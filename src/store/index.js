const initStore = function(){
    return{
        changeUserToPosts: null,
        changeUserToTodos: null,
        changeUserToComments: null,
        changeCommentsToCommentPut: null,

        item: {
            "email": "",
            "name": "",
            "gender": "",
            "status": "",
          },
    }
}

const storeApp = {
    state: {
        appStore: {...initStore()}
    },
    mutations: {
            setPostUser (state, getUser) {
              state.appStore.item.email = getUser.email
              state.appStore.item.name = getUser.name
              state.appStore.item.gender = getUser.gender
              state.appStore.item.status = getUser.status
            },
            
            changeUserToPosts(state , UserToPosts){
              state.appStore.changeUserToPosts = UserToPosts
            },
            changeUserToTodos(state , ChangeToTodos){
              state.appStore.changeUserToTodos = ChangeToTodos
            },
            changeUserToComments(state, ChangeToComments){
              state.appStore.changeUserToComments = ChangeToComments
            },
            changeCommentsToCommentPut(state, commentsPut){
              state.appStore.changeCommentsToCommentPut = commentsPut
            }
            
    },
    getters: {
        itemStore: state => state.appStore.item,
        UserToPosts:state => state.appStore.changeUserToPosts,
        PostsToComments: state => state.appStore.changeUserToComments,
        commentsToCommentPut: state => state.appStore.changeCommentsToCommentPut,
        UserToTodos: state => state.appStore.changeUserToTodos
    }
}

export default storeApp