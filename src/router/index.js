import { createWebHistory, createRouter } from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Comments from '../views/Comments.vue'
import Todos from '../views/Todos.vue'
import Posts from '../views/Posts.vue'

const routes = [

    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/about",
        name: "About",
        component: About
    },
    {
        path: "/comments",
        name: "Comments",
        component: Comments
    },
    {
        path: "/posts",
        name: "Posts",
        component: Posts
    },
    {
        path: "/todos",
        name: "Todos",
        component: Todos
    },


]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router