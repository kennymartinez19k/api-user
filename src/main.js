import 'es6-promise/auto'
import { createApp } from 'vue'
import { createStore } from 'vuex'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import Loading from 'vue3-loading-overlay'
import App from './App.vue'
import router from './router'
import storeModules from './store'
import Toaster from '@meforma/vue-toaster'



const store = createStore({ ...storeModules })

library.add(fas)

createApp(App).component('fa', FontAwesomeIcon).component('loading' , Loading).use(Toaster).use(router).use(store).mount('#app')
